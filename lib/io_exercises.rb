# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.



def guessing_game
  random_array = (1..100).to_a.shuffle
  random_num = random_array[0]
  guesses = 1

  puts "Guess a number"
  input = gets.chomp.to_i
  until input == random_num
    if input < random_num
      puts "#{input} is too low"
      guesses += 1
    elsif input > random_num
      puts "#{input} is too high"
      guesses += 1
    end
    input = gets.chomp.to_i
  end
  p "#{input} \n #{guesses}"
end
